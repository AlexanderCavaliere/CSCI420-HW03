# HW03_CAVALIERE_Alexander_program.py
# Alexander R. Cavaliere <arc6393@rit.edu>
# 2017-02-19

# Allows us to read/write csv files
import csv
# Used for mean() function (I'm lazy :shrug:)
import statistics
# Used for binning the data for the histogram
import numpy
# Used for generating the histogram and plots
import matplotlib.pyplot as plt

# Floating Point Range Function
def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step 

# Classifies the reckless drivers through the minimization
# of the false alarm rate.
class Highway_Recklessness_Classification:

    # Reads in the csv file
    def Read_Data( filename ):
        data = []
        # Read in the CSV file 
        with open( filename, newline='' ) as csvfile:
            reader = csv.reader( csvfile, dialect='excel', delimiter=',', quotechar='\'' )
            # Go row by row and store each value in our data list
            for row in reader:
                if row[0] == "SPEED": # Skip the labels
                    continue
                data.append( (float(row[0].replace( " ", "" )), int(row[1].replace( " ", ""))) ) # Make a list of tuples (speed, reckless bit)
        data.sort() # Sort the data
        rounded_and_sorted_speeds = [] # List to store the rounded speed data and their reckless flag
        for speed in data: # Take the sorted speeds and round them to the closest 0.5 mark
            rounded_and_sorted_speeds.append( (round( speed[0]/ 0.5 ) * 0.5, speed[1]) )
        return rounded_and_sorted_speeds # Return the list so we use it later

    # Threshold Calculation
    def Find_Best_False_Positive_Rate( sorted_data_points ):
        best_false_positive_rate = float('inf') # Set the initial best variances to infinity
        best_threshold = -1 # Set the speed threshold to a negative value
        false_positive_rates = [] # Empty list to store the fp rates 
        true_positive_rates = [] # Empty list to store the tp rates
        speeds = [] # We'll store the speeds in their own list
        thresholds = [] # All of the thresholds we'll loop over (We'll need 'em for our graph)
        for speed in sorted_data_points: # Pull the speeds out of the list of tuples
            speeds.append( speed[0] )
        for threshold in frange( min(speeds), max(speeds), 0.5 ): #Starting at the lowest speed to the highest by 0.5 MPH steps
            speeds_above = [] # List to store speeds above threshold
            speeds_below = [] # List to store speeds below threshold
            true_positives = 0 # The true_positives for each threshold
            false_alarms = 0 # The false alarms for each threshold
            false_negatives = 0 # The false negatives for each treshold
            for speed_and_speeding_mark in sorted_data_points: # Loop over each of the collected speeds
                if speed_and_speeding_mark[0] > threshold: # If the speed is above the threshold
                    speeds_above.append( speed_and_speeding_mark ) # Add it to the speeds above
                    if speed_and_speeding_mark[1] == 1: # Check if the driver was marked as reckless
                        true_positives +=1 # And record it if is true
                    else:
                        false_alarms += 1 # Otherwise we have fouled
                elif speed_and_speeding_mark[0] <= threshold: # If the speed is below the threshold
                    speeds_below.append( speed_and_speeding_mark ) # Add it to the speeds below
                    if speed_and_speeding_mark[1] == 0: # Check if the driver was marked as safe
                        true_positives += 1 # And record it as a true positive
                    else: 
                        false_negatives += 1 # Otherwise it's a false negative.

            # Calculate the false positive rate
            false_positive_rate = false_alarms / ( false_alarms + true_positives )
            # Calculate the true positive rate (For a ROC curve) and add it to the list
            true_positive_rates.append( true_positives / ( false_negatives + true_positives ) )
            # Add the false positive rate and threshold to their appropriate lists
            false_positive_rates.append( false_positive_rate )
            thresholds.append( threshold ) 
            # Check if we have found a better false positive rate 
            if false_positive_rate < best_false_positive_rate:
                best_false_positive_rate = false_positive_rate
                best_threshold = threshold
            # Calculate the miss fire rate 
            
        # Print out our findings
        print( best_false_positive_rate ) 
        print( best_threshold )
        # Return the generated data 
        return ( best_threshold, best_false_positive_rate, false_positive_rates, thresholds, true_positive_rates )
        
    # Plot the Bin Speeds compared to the Weighted Variances
    def Make_False_Alarm_Speed_Plot( data, filename ):
        best_threshold = data[0]
        best_false_positive_rate = data[1]
        false_positive_rates = data[2]
        thresholds = data[3]
        markers_on = []
        # Finds the points with the lowest misclassification rates and flags them
        for position in range( 0, len( false_positive_rates ) ):
            if false_positive_rates[position] == min( false_positive_rates ):
                markers_on.append( position )
                
        # Clear the original Plot and Plot the False Positive Rate function
        plt.clf()
        plt.plot( thresholds, false_positive_rates, "-bD" )
        plt.plot( thresholds, false_positive_rates, "rx", markevery = markers_on )
        # Add the labels
        plt.xlabel( "Vehicle Speed Threshold (MPH)" )
        plt.ylabel( "False Positive Rate" )
        plt.title( "False Positive Rate of each Threshold" )
        # Save the figure
        plt.savefig( filename ) # Name of file.png

    # Plot a ROC curve for the data
    def Make_ROC_Curve( data, filename ):
        false_positive_rates = data[2]
        true_positive_rates = data[4]
        markers_on = []
        # Finds the points with the lowest misclassification rate and flags them
        for position in range( 0, len( false_positive_rates ) ):
            if false_positive_rates[position] == min( false_positive_rates ):
                markers_on.append( position )
                
        # Clear the plot and plot the ROC curve
        plt.clf()
        plt.plot( false_positive_rates, true_positive_rates, "-bD" )
        plt.plot( false_positive_rates, true_positive_rates, "rx", markevery = markers_on )
        # Add the labels
        plt.xlabel( "False Positive Rate" )
        plt.ylabel( "True Positive Rate" )
        plt.title( "ROC Curve for Reckless Driver Data" )
        # Save the figure
        plt.savefig( filename )

    def Find_Threshold_Failure_Amount( binarized_data, sorted_data ):
        best_threshold = binarized_data[0]
        missed_reckless_drivers = 0
        for speed in sorted_data:
            if speed[0] > best_threshold and speed[1] == 0:
                missed_reckless_drivers += 1
        print( missed_reckless_drivers )
        print( len( sorted_data ) )

# Instantiate the class to perform clustering method
HighwayRecklessness = Highway_Recklessness_Classification

# Deal with the speed data
data = HighwayRecklessness.Read_Data( "CLASSIFIED_TRAINING_SET_FOR_RECKLESS_DRIVERS.csv" ) # Read in the data
# Perform OTSU's method on the speed histogram
binarized_data = HighwayRecklessness.Find_Best_False_Positive_Rate( data )
# Make the best threshold and lowest variance on histogram
HighwayRecklessness.Make_False_Alarm_Speed_Plot( binarized_data, "FP_Rate_VS_Thresholds_Plot.png" )
HighwayRecklessness.Make_ROC_Curve( binarized_data, "ROC_Curve.png" )
HighwayRecklessness.Find_Threshold_Failure_Amount( binarized_data, data )
